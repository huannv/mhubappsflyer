package vn.mwork.mhubappsflyer;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class ActivityTwo extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		Toast.makeText(getApplicationContext(), "Main App", Toast.LENGTH_SHORT)
				.show();
	}

	public void onClick(View view) {
		finish();
	}

	@Override
	public void finish() {
		super.finish();
	}
}

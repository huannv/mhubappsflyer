package vn.mwork.mhubappsflyer;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.appsflyer.AppsFlyerLib;
import com.facebook.AppEventsLogger;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;

public class MHubAppsflyer extends Activity {

	private static final String	FORWARD_ACTIVITY	= "vn.appsflyer.activity";
	private static final String	DEV_KEY				= "vn.mwork.appsflyer.devkey";

	private Context				context;

	private Activity			activity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		context = this.getApplicationContext();
		activity = MHubAppsflyer.this;

		if (savedInstanceState != null) {
			finish();
		} else {
			try {
				String devKey = getvalueFromMetaData(DEV_KEY);
				AppsFlyerLib.setAppsFlyerKey(devKey);
				AppsFlyerLib.setCurrencyCode("USD");
				AppsFlyerLib.setUseHTTPFalback(false);
				AppsFlyerLib.sendTracking(getApplicationContext());
				if (!hasFacebook()) {
					getTokenFace();
				}

				String forwardActivity = getvalueFromMetaData(FORWARD_ACTIVITY);
				Intent intent = new Intent();
				intent.setClassName(this, forwardActivity);
				startActivity(intent);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (activityRunningSize() == 2) {
			finish();
		} else {
			if (!hasFacebook()) {
				String key = getvalueFromMetaData("com.facebook.sdk.ApplicationId").replace("a", "");
				AppEventsLogger.activateApp(this, key);
			}
		}
	}

	private int activityRunningSize() {
		int count = 0;
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		List<ActivityManager.RunningTaskInfo> task = manager.getRunningTasks(1);
		count = task.get(0).numActivities;

		return count;
	}

	/**
	 * Get value from meta data tag of manifest
	 */
	public String getvalueFromMetaData(String key) {
		String receiver = "";
		try {
			ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(),
					PackageManager.GET_META_DATA);
			Bundle bundle = ai.metaData;
			receiver = bundle.getString(key);
		} catch (NameNotFoundException e) {
			Log.e("MHubAppsflyer", "Failed to load meta-data, NameNotFound: " + e.getMessage());
		} catch (NullPointerException e) {
			Log.e("MHubAppsflyer", "Failed to load meta-data, NullPointer: " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return receiver;
	}

	public boolean hasFacebook() {
		boolean receiver = false;
		try {
			ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(),
					PackageManager.GET_META_DATA);
			Bundle bundle = ai.metaData;
			receiver = bundle.getBoolean("vn.mwork.appsflyer.nofacebook");
		} catch (NameNotFoundException e) {
			Log.e("MHubAppsflyer", "Failed to load meta-data, NameNotFound: " + e.getMessage());
		} catch (NullPointerException e) {
			Log.e("MHubAppsflyer", "Failed to load meta-data, NullPointer: " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return receiver;
	}

	private void getTokenFace() {
		Session.openActiveSession(activity, true, new Session.StatusCallback() {

			// callback when session changes state
			@SuppressWarnings("deprecation")
			@Override
			public void call(final Session session, SessionState state, Exception exception) {
				Log.d("TokenFaceBook", "TokenFaceBook : " + session.isOpened());
				if (session.isOpened()) {

					Session.setActiveSession(session);
					Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {

						@Override
						public void onCompleted(GraphUser user, Response response) {

						}
					});

				}
			}
		});
	}

	private void getHashKey() {
		PackageInfo info;
		try {
			info = getPackageManager().getPackageInfo("vn.mwork.mhubappsflyer", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md;
				md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				String something = new String(Base64.encode(md.digest(), 0));
				// String something = new
				// String(Base64.encodeBytes(md.digest()));
				Log.e("hash key", something);
			}
		} catch (NameNotFoundException e1) {
			Log.e("name not found", e1.toString());
		} catch (NoSuchAlgorithmException e) {
			Log.e("no such an algorithm", e.toString());
		} catch (Exception e) {
			Log.e("exception", e.toString());
		}
	}

}
